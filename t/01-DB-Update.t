# Before `make install' is performed this script should be runnable with
# `make test'. After `make install' it should work as `perl DB-Update-Single.t'

# $Ringlet$

#########################

# change 'tests => 1' to 'tests => last_test_to_print';

use Test::More tests => 3;

#########################

# Insert your test code below, the Test::More module is use()ed here so read
# its man page ( perldoc Test::More ) for help writing this test script.

sub test_use($);

sub test_use($)
{
	my ($modname) = @_;

	eval "use $modname";
	if ($@) {
		diag("\n\"use $modname\" failed, maybe you are missing some of the prerequisite Perl modules?");
		return 0;
	}
	return 1;
}

ok(test_use('DB::Update'));
ok(test_use('DB::Update::Parse'));
ok(test_use('DB::Update::Shell'));
