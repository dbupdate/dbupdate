#!/usr/bin/perl -Tw

use strict;

package DB::Update::Batch;

use DB::Update::Output qw/:dbo/;

=pod

=head1 NAME

DB::Update::Batch - manipulate a single query batch of database updates

$Ringlet$

=head1 SYNOPSIS

    use DB::Update::Batch;

    $b = new DB::Update::Batch;
 
    $b->id('populate');
    $b->add_query($q1);
    $b->add_query($q2);

    print $b->id();
    foreach $q ($b->queries()) { print $q->id(); }

=head1 DESCRIPTION

C<DB::Update::Batch> is a class intended for handling a single query batch
in a database update.  It is usually accessed through the C<batches()>
method of the C<DB::Update::Single> class.

A query batch consists of one or more queries that are to be executed
sequentially.  In some SQL server implementations, such as Microsoft SQL
Server, query batches are the smallest execution unit that e.g. an execution
plan is constructed for, so it makes sense to group as many queries as
possible in a single batch.  Certain queries, such as C<ALTER TABLE>, may
only be executed as a first query in a batch.

In the DBXML file format, a query batch is represented by the
C<sql-batch> element.

=head1 CONSTRUCTOR

=over 4

=item new

This is the constructor for a new DB::Update::Batch object.  It takes no
arguments in the present version.

=back

=cut

sub new
{
	my $proto = shift;
	my $class = ref($proto) || $proto;
	my $self = { };

	$self->{'id'} = undef;
	$self->{'queries'} = [ ];
	bless $self, $class;
	return $self;
}

=pod

=head1 METHODS

=over 4

=item add_query (QUERY)

Add a query to the end of the batch.  The query should be an object from a
class derived from C<DB::Update::Query>.

=cut

sub add_query($ $)
{
	my ($self, $q) = @_;

	push @{$self->{'queries'}}, $q;
}

=pod

=item queries

Return a list of the queries in the current batch as C<DB::Update::Query>
objects.

=cut

sub queries($)
{
	my ($self) = @_;

	return @{$self->{'queries'}};
}

=pod

=item id ([ID])

Get or set the string identifier of the current batch, represented by the
C<id> attribute of the C<sql-batch> element in the DBXML file format.

=cut

sub id($ $)
{
	my ($self, $id) = @_;

	$self->{'id'} = $id if (defined($id));
	return $self->{'id'};
}

=pod

=item reset

Free all resources allocated for the current batch and its queries.

=cut

sub reset($)
{
	my ($self) = @_;

	$_->reset() for @{$self->{'queries'}};
	$self->{'id'} = $self->{'queries'} = undef;
}

=pod

=item output

Output a DBXML representation of the current batch.

=cut

sub output($)
{
	my ($self) = @_;

	dbo_output(sprintf('    <sql-batch id="%s">',
	    dbo_escape($self->{'id'})));
	$_->output() for @{$self->{'queries'}};
	dbo_output('    </sql-batch>');
}

=pod

=back

=head1 AUTHORS

The C<DB::Update::Batch> module was written by Peter Pentchev
<roam@ringlet.net> in 2004.

=cut

1;
