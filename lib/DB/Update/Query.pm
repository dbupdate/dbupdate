#!/usr/bin/perl -Tw

use strict;

package DB::Update::Query;

# $Ringlet$

use DB::Update::Output qw/:dbo/;

sub new
{
	my $proto = shift;
	my $class = ref($proto) || $proto;
	my $self = { };

	$self->{'name'} = '';
	$self->{'text'} = '';
	bless $self, $class;
	return $self;
}

sub name($ $)
{
	my ($self, $name) = @_;

	$self->{'name'} = $name if (defined($name));
	return $self->{'name'};
}

sub text($ $)
{
	my ($self, $text) = @_;

	$self->{'text'} = $text if (defined($text));
	return $self->{'text'};
}

sub reset($)
{
	my ($self) = @_;

	$self->{'name'} = $self->{'text'} = undef;
}

sub output($)
{
	my ($self) = @_;

	dbo_output(sprintf('      <sql-query name="%s">',
	    dbo_escape $self->{'name'}));
	print dbo_escape($self->{'text'})."\n";
	dbo_output('      </sql-query>');
}

1;
