#!/usr/bin/perl -Tw

use strict;

package DB::Update::Parse;

# $Ringlet$

use DB::Update::Migration;
use DB::Update::Single;
use DB::Update::Batch;
use DB::Update::Query;

use XML::Parser;

my ($filename, $mig, $upd, $batch, $q, $str);
my @elstack = ();

sub handle_start($ $ @);
sub handle_end($ $);
sub handle_char($ $);

sub parse($)
{
	my ($p, $text, $res) = ();
	
	$p = new XML::Parser(Handlers => {
	    Start => \&handle_start,
	    End => \&handle_end,
	    Char => \&handle_char });
	$mig = $upd = $batch = $q = undef;
	$str = '';
	$filename = $_[0];
	@elstack = ();
	open(F, "< $filename") or die("Opening $filename: $!\n");
	if (-d F) {
		close(F);
		die("$filename is a directory!\n");
	}
	$text = '';
	while (defined($_ = <F>)) {
		s/^-- DBXML: //;
		$text .= $_;
	}
	close F;
	$p->parse($text);
	return $mig;
}

sub start_migration($ $ @)
{
	$mig = new DB::Update::Migration();
	$mig->filename($filename);
}

sub start_update($ $ %)
{
	my ($parser, $el, %attrs) = @_;

	$upd = new DB::Update::Single();
	for (qw(product version comment)) {
		die("Missing update attribute: $_")
		    unless (defined($attrs{$_}));
		$upd->set($_, $attrs{$_});
	}
	$upd->set('desc', '');
}

sub end_update($ $)
{
	my ($p, $el) = @_;

	$mig->add_update($upd);
	$upd = undef;
}

sub text_comments($ $)
{
	my ($el, $text) = @_;
	
	$upd->{'desc'} .= $text;
}

sub start_batch($ $ @)
{
	my ($parser, $el, %attrs) = @_;

	die("Batch without an id") unless (defined($attrs{'id'}));
	$batch = new DB::Update::Batch;
	$batch->id($attrs{'id'});
}

sub start_query($ $ @)
{
	my ($parser, $el, %attrs) = @_;

	die("Query without a name") unless (defined($attrs{'name'}));
	$q = new DB::Update::Query;
	$q->name($attrs{'name'});
}

sub text_query($ $)
{
	my ($parser, $text) = @_;

	$q->text($q->text().$text);
}

sub end_query($ $)
{
	my ($parser, $el) = @_;

	$batch->add_query($q);
	$q = undef;
}

sub end_batch($ $)
{
	my ($parser, $el) = @_;

	$upd->add_batch($batch);
	$batch = undef;
}

my %start_handlers = (
	'db-migration'	=> \&start_migration,
	'db-update'	=> \&start_update,
	'sql-batch'	=> \&start_batch,
	'sql-query'	=> \&start_query,
);

my %text_handlers = (
	'comments'	=> \&text_comments,
	'sql-query'	=> \&text_query,
);

my %end_handlers = (
	'db-update'	=> \&end_update,
	'sql-query'	=> \&end_query,
	'sql-batch'	=> \&end_batch,
);

sub handle_start($ $ @)
{
	my ($p, $el, @attr) = @_;
	my ($last, $h);
	
	# Call the text handler if needed
	if (($str ne '') && @elstack) {
		$last = $elstack[$#elstack];
		&{$text_handlers{$last}}($last, $str)
		    if (defined($text_handlers{$last}));
	}
	$str = '';

	&{$start_handlers{$el}}($p, $el, @attr)
	    if (defined($start_handlers{$el}));
	push @elstack, $el;
}

sub handle_char($ $)
{
	my ($p, $text) = @_;

	$str .= $text;
}

sub handle_end($ $)
{
	my ($p, $el) = @_;
	my $last;

	# Sanity check
	die("Internal error: empty elstack on end($el)") unless (@elstack);
	$last = pop @elstack;
	die("Internal error: elstack popped $last, end $el")
	    unless ($el eq $last);

	# Call the text handler if needed
	&{$text_handlers{$el}}($el, $str)
	    if (($str ne '') && defined($text_handlers{$el}));
	$str = '';

	# Call the handlers
	&{$end_handlers{$el}}($p, $el) if (defined($end_handlers{$el}));
}

1;
