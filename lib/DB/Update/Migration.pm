#!/usr/bin/perl -Tw

use strict;

use File::Basename;

use DB::Update::Single;

package DB::Update::Migration;

# $Ringlet$

use DB::Update::Output qw/:dbo/;

sub new
{
	my $proto = shift;
	my $class = ref($proto) || $proto;
	my $self = {};

	$self->{'FNAME'} = undef;
	$self->{'BASENAME'} = undef;
	$self->{'UPDATES'} = [ ];
	bless $self, $class;
	return $self;
}

sub filename($ $)
{
	my ($self, $name) = @_;

	if (defined($name)) {
		$self->{'FNAME'} = $name;
		$self->{'BASENAME'} = ::basename($name);
	}
	return $self->{'FNAME'};
}

sub basename($)
{
	my ($self) = @_;

	return $self->{'BASENAME'};
}

sub updates($)
{
	my $self = $_[0];
	return @{$self->{'UPDATES'}};
}

sub add_update($ $)
{
	my ($self, $update) = @_;
	push @{$self->{'UPDATES'}}, $update;
}

sub reset($)
{
	my $self = $_[0];
	
	$self->{'FNAME'} = undef;
	$self->{'BASENAME'} = undef;
	$_->reset() for @{$self->{'UPDATES'}};
	$self->{'UPDATES'} = [ ];
}

sub output($)
{
	my $self = $_[0];

	dbo_output('<?xml version="1.0" encoding="utf-8"?>');
	dbo_output('<db-migration xmlns="http://schemas.cnsys.bg/dbmigrate/2004/1/">');
	$_->output() for @{$self->{'UPDATES'}};
	dbo_output('</db-migration>');
}

1;
