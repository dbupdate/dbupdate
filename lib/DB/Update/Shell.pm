#!/usr/bin/perl -Tw

use strict;

use Carp;
use DBI;
use File::Basename;
use Text::ParseWords;

use DB::Update::Migration;
use DB::Update::Parse;

package DB::Update::Shell;

=pod

=head1 NAME

DB::Update::Shell - a generic shell for applying database updates

$Ringlet$

=head1 SYNOPSIS

  use DB::Update::Shell;
  use DB::Update::Shell::I<custom>;

  # Create a shell of a type I<derived from> DB::Update::Shell
  $sh = DB::Update::Shell::I<custom>->new()

  # Let the shell load a migration file
  $sh->add_migration($fname...);

  # Run the shell's loop for reading and executing user commands
  $sh->loop()

=head1 DESCRIPTION

The C<DB::Update::Shell> class provides an abstract interface for working
with the C<DB::Update> class hierarchy - parsing database update files,
examining the current database schema and applying updates.

I<The C<DB::Update::Shell> class may not be instantiated directly!>

The C<DB::Update::Shell> class contains only the abstract methods and the
code for parsing and executing user commands.  It should not be instantiated
directly; create an object from one of the derived classes, like
C<DB::Update::Shell::Console>, instead.

=cut

my %settings = (
	'dbdriver'	=> 'Pg',
	'dbconn'	=> 'dbname=billing',
	'dbuser'	=> 'pgsql',
	'dbpass'	=> '',
	'dbschema'	=> 'dbupdate',
	'dbtable'	=> 'software_versions',
);

my %helpstr = (
	'exit'		=> 'Leave the DBUpdate assistant.',
	'help'		=> 'Display help about the available commands.',
	'list'		=> 'Display the available migrations or updates.',
	'leave',	=> 'Go one level up.',
	'nop'		=> 'Do nothing.',
	'set'		=> 'Change the value of a variable.',
	'show'		=> 'Display the value of a variable or all vars.',
	'select'	=> 'Select an update so it will be applied.',
	'unselect'	=> 'Unselect an update so it will not be applied.',
);

my %tabledefs = (
	'GENERIC'	=> q{ (
		VerProduct	VARCHAR(64) NOT NULL,
		VerVersion	VARCHAR(30) NOT NULL,
		VerComment	TEXT NOT NULL,
		VerUpdated	DATETIME NOT NULL)},
	'Pg'		=> q{ (
		VerProduct	VARCHAR(64) NOT NULL,
		VerVersion	VARCHAR(30) NOT NULL,
		VerComment	TEXT NOT NULL,
		VerUpdated	TIMESTAMP WITH TIME ZONE NOT NULL)},
);

sub all_help($ @);
sub all_quit($ @);
sub top_list($ @);
sub mig_list($ @);
sub mig_leave($ @);
sub upd_leave($ @);
sub all_nop($ @);
sub all_set($ @);
sub all_testconn($ @);
sub top_exam($ @);
sub mig_exam($ @);
sub mig_exam_real($ $ $);
sub all_run($ @);
sub all_testrun($ @);
sub all_reload($ @);
sub mig_select($ @);
sub mig_unselect($ @);
sub mig_real_select($ $ $ @);

sub qfatal($ $);

my %commands = (
	'apply'		=> 'run',
	'examine'	=> [ \&top_exam, \&mig_exam, undef ],
	'execute'	=> 'run',
	'exit'		=> \&all_quit,
	'help'		=> \&all_help,
	'info'		=> \&all_info,
	'list'		=> [ \&top_list, \&mig_list, undef ],
	'load'		=> \&all_load,
	'leave'		=> [ undef, \&mig_leave, \&upd_leave ],
	'nop'		=> \&all_nop,
	'quit'		=> 'exit',
	'prepare'	=> \&all_prepare,
	'reload'	=> \&all_reload,
	'run'		=> \&all_run,
	'select'	=> [ \&top_select, \&mig_select, undef ],
	'set'		=> \&all_set,
	'show'		=> \&all_show,
	'testconn'	=> \&all_testconn,
	'testrun'	=> \&all_testrun,
	'unload'	=> \&all_unload,
	'unselect'	=> [ \&top_unselect, \&mig_unselect, undef ],
	'up'		=> 'leave',
);

=pod

=head1 METHODS

The C<DB::Update::Shell> class contains the following methods:

=over 4

=item * new

Create a new C<DB::Update::Shell> object.

I<Do not invoke this method directly!>  Use one of the derived classes
instead.

=cut

sub new
{
	my $proto = shift;
	my $class = ref($proto) || $proto;
	my $self = { };

	$self->{'MIG'} = [ ];
	$self->{'PRODUCTS'} = { };
	$self->{'CMIG'} = undef;
	$self->{'CUPD'} = undef;
	$self->{'GETLINE'} = undef;
	$self->{'SQL'} = { };
	$self->{'DEBUG'} = 0;
	$self->{'BATCH'} = 0;
	$self->{'TEST_RUN'} = 0;
	bless $self, $class;
	return $self;
}

=pod

=item * debug($level, $message) [overridden]

A derived class must override the C<debug> method, checking the C<$level>
and displaying the specified C<$message> in an appropriate fashion if
needed.

=cut

sub debug($ $ $)
{
	::croak("DB::Update::Shell->debug() must be overridden!");
}

sub debug_enable($ $)
{
	my ($self, $flag) = @_;

	$self->{'DEBUG'} = $flag if defined($flag);
	return $self->{'DEBUG'};
}

sub batch($ $)
{
	my ($self, $flag) = @_;

	$self->{'BATCH'} = $flag if defined($flag);
	return $self->{'BATCH'};
}

=item * parse_opts($optref)

Apply the settings passed in the hash referenced by B<$optref> to
the B<%settings> hash.

=cut

sub parse_opts($ $)
{
	my ($self, $opts) = @_;

	$self->debug(1, "parse_opts called\n");
	foreach (keys %{$opts}) {
		if (exists($settings{$_})) {
			$self->debug(1, "- setting $_ to ".$opts->{$_}."\n");
			$settings{$_} = $opts->{$_};
		} else {
			$self->debug(1, "- unknown option $_, bailing out\n");
			$self->err("Unknown option $_\n");
			return 0;
		}
	}
	$self->debug(1, "parse_opts finised, returning 1\n");
	return 1;
}

sub err($ $)
{
	::croak("DB::Update::Shell->err() must be overridden!");
}

sub warn($ $)
{
	::croak("DB::Update::Shell->warn() must be overridden!");
}

sub getline($ $ $ $)
{
	::croak("DB::Update::Shell->getline() must be overridden!");
}

sub level($)
{
	my ($self) = @_;

	return scalar(grep {defined($_)} ($self->{'CMIG'}, $self->{'CUPD'}));
}

sub unalias($)
{
	my $cmd = $_[0];
	my $c;
	
	$c = $commands{$cmd};
	while (defined($c) && ref($c) eq '') {
		die("Internal error: loop command '$cmd'\n") if ($cmd eq $c);
		$cmd = $c;
		$c = $commands{$cmd};
	}
	return $cmd;
}

sub forlevel($)
{
	my ($self, $cmd) = @_;
	my $c;

	$c = $commands{unalias($cmd)};
	return $c unless (defined($c));
	return $c if (ref($c) eq 'CODE');
	die("Internal error: invalid command '$cmd'\n") unless (ref($c) eq 'ARRAY');
	return $c->[$self->level()];
}

sub select_mig($ $)
{
	my ($self, $sel) = @_;

	if ($sel =~ /^\d+$/) {
		if ($sel >= 1 && $sel <= @{$self->{'MIG'}}) {
			return $self->{'MIG'}->[$sel - 1];
		}
	} else {
		foreach my $m (@{$self->{'MIG'}}) {
			return $m if ($m->filename() eq $sel ||
			    $m->basename() eq $sel);
		}
	}
	return undef;
}

sub dbh($ $ @)
{
	my ($self, $referr, @args) = @_;
	my ($data, $u, $p, $dbh, $err);

	($data, $u, $p) = ($settings{'dbconn'}, $settings{'dbuser'},
	    $settings{'dbpass'});
	$data = $args[0] if ($#args >= 0);
	$u = $args[1] if ($#args >= 1);
	$p = $args[2] if ($#args >= 2);

	$data = "dbi:$settings{dbdriver}:$data";
	$self->debug(1, "dbh dbconn '$data', username '$u', pass '$p'\n");
	$err = undef;
	$dbh = undef;
	$self->{'SQL'} = { };
	eval {
		$dbh = DBI->connect($data, $u, $p);
		$err = DBI->errstr() unless (defined($dbh));
	};
	if ($@) {
		$err = $@;
	}
	$err = 'Unknown error' unless (defined($dbh) || defined($err));
	${$referr} = $err if (defined($referr));
	return $dbh;
}

sub dbtable($)
{
	my ($self) = @_;

	if ($settings{'dbschema'} eq '') {
		return $settings{'dbtable'};
	} else {
		return $settings{'dbschema'}.'.'.$settings{'dbtable'};
	}
}

sub get_info($ $)
{
	my ($self, $dbh) = @_;
	my ($err, $sth, @row, %warned);
	my %res;

	$sth = $dbh->prepare(
	    'SELECT VerProduct, VerVersion, VerComment, VerUpdated FROM '.
	    $self->dbtable());
	if (!defined($sth)) {
		$self->warn("Could not prepare query: ".$dbh->errstr());
		return undef;
	}
	if (!$sth->execute()) {
		$self->warn("Error executing query: ".$dbh->errstr());
		$sth->finish();
		return undef;
	}
	%res = ();
	while (@row = $sth->fetchrow_array()) {
		if (exists($res{$row[0]})) {
			unless ($warned{$row[0]}) {
				$self->warn("The database contains several ".
				    "definitions for product $row[0]!");
				$warned{$row[0]} = 1;
			}
			$self->debug(1, "comparing ".$res{$row[0]}->[1].
			    " against ".$row[1]);
			next if $res{$row[0]}->[1] gt $row[1];
			$self->debug(1, "apparently a newer version found");
		}
		$res{$row[0]} = [ ];
		push @{$res{$row[0]}}, @row, 0;
	}
	$sth->finish();
	return \%res;
}

sub loop($ @)
{
	my ($self, @args) = @_;
	my ($yet, $fatal, $prompt, $line, $cmd, @sel, $c, $pos);
	my @queued;
	
	all_load($self, @args) if (@args);

	if ($self->{'BATCH'}) {
		push @queued, 'up' if defined($self->{'CMIG'});
		push @queued, 'up' if defined($self->{'CUPD'});
		push @queued, ('prepare', 'info', 'examine all', 'apply');
	}

	$yet = 1;
	while ($yet) {
		$prompt = '';
		$prompt .= $self->{'CMIG'}->filename()
		    if (defined($self->{'CMIG'}));
		$prompt .= sprintf('/%s-%s', $self->{'CUPD'}->{'product'},
		    $self->{'CUPD'}->{'version'})
		    if (defined($self->{'CUPD'}));
		if (@queued) {
			$line = shift @queued;
			$fatal = 1;
			delete $self->{'FATALERR'};
		} else {
			last if $self->{'BATCH'};
			$line = $self->getline($prompt, $self->{'CMIG'},
			    $self->{'CUPD'});
			$fatal = 0;
		}
		$self->{'FATAL'} = $fatal;
		$line =~ s/[\s\r\n]*$//;
		last unless (defined($line));
		@args = ::parse_line('\s+', 0, $line);
		$self->debug(1, "Broke $line into (\"".join('", "', @args)."\")\n");
		next unless (@args);
		$cmd = shift @args || '';
		$self->debug(1, "command is $cmd\n");

		@sel = grep /^\Q$cmd\E/, keys(%commands);
		if ($#sel == -1) {
			$self->warn("Unrecognized command '$cmd'");
			return 0 if $fatal;
			next;
		} elsif ($#sel > 0) {
			$self->warn("Ambiguous command '$cmd': '".join("', '", @sel)."'");
			return 0 if $fatal;
			next;
		}
		# FIXME: actually execute the command
		$self->debug(1, "about to exec command $sel[0], ref(c) is ".ref($c)."\n");
		$c = $commands{$sel[0]};
		while (ref($c) eq '') {
			$self->debug(1, "$sel[0] seems to be an alias for '$c'\n");
			if ($sel[0] eq $c) {
				$self->warn("Internal error: loop for command $c");
				$c = \&all_nop;
				return 0 if $fatal;
				last;
			}
			$sel[0] = $c;
			$c = $commands{$sel[0]};
		}
		if (ref($c) eq 'CODE') {
			$self->debug(1, "one for all and all for one\n");
			$yet = &$c($self, @args);
		} else {
			$pos = scalar(grep {defined($_)} ($self->{'CMIG'}, $self->{'CUPD'}));
			$self->debug(1, "pos is $pos\n");
			if (!defined($c->[$pos])) {
				$self->warn("Command '$sel[0]' undefined in current context");
				next;
			} elsif (ref($c->[$pos]) ne 'CODE') {
				$self->warn("Internal error: ref($sel[0], $pos) is not CODE but ".ref($c->[$pos]));
				next;
			}
			$yet = &{$c->[$pos]}($self, @args);
		}
	}
	$self->debug(1, "Reached the end, it seems.\n");
	if ($self->{'FATAL'} && exists($self->{'FATALERR'})) {
		delete $self->{'FATALERR'};
		$self->debug(1, "FATALERR set, signalling a fatal error!\n");
		return 0;
	}
	return 1;
}

sub qfatal($ $)
{
	my ($self, $retval) = @_;

	if ($self->{'FATAL'}) {
		$self->{'FATALERR'} = 1;
		return 0;
	}
	return $retval;
}

sub top_list($ @)
{
	my ($self, @args) = @_;
	my ($sel, $m, $res);

	# List a migration if specified
	if (@args) {
		$sel = shift @args;
		$m = $self->select_mig($sel);
		if (!defined($m)) {
			$self->warn("Invalid migration $sel");
			return $self->qfatal(1);;
		}

		$self->{'CMIG'} = $m;
		$res = mig_list($self, @args);
		$self->{'CMIG'} = undef;
		return $res;
	}

	# OK, no arguments, just list the migrations
	foreach my $m (@{$self->{'MIG'}}) {
		print "Migration: ".$m->filename()."\n";
	}
	return 1;
}

sub mig_list($ @)
{
	my ($self, @args) = @_;

	# No arguments used, just list the updates
	foreach my $u ($self->{'CMIG'}->updates()) {
		printf("\t%s %s-%s\t%s\n", $u->{'selected'}, $u->{'product'},
		    $u->{'version'}, $u->{'comment'});
	}
	return 1;
}

sub mig_leave($ @)
{
	my ($self, @args) = @_;

	$self->{'CMIG'} = undef;
	return 1;
}

sub upd_leave($ @)
{
	my ($self, @args) = @_;

	$self->{'CUPD'} = undef;
	return 1;
}

sub all_quit($ @)
{

	return 0;
}

sub all_nop($ @)
{

	return 1;
}

sub all_help_cmd($ $ $)
{
	my ($self, $cmd, $single) = @_;
	my $c;

	$c = $self->forlevel($cmd);
	if (!defined($c)) {
		if ($single) {
			$self->warn("The $cmd command is not available at this level.");
			return $self->qfatal(1);
		}
		return 1;
	}
	if (ref($commands{$cmd}) eq '') {
		print "\t$cmd\tAlias for '$commands{$cmd}'.\n";
		return 1;
	}
	if (defined($helpstr{$cmd})) {
		print "\t$cmd\t$helpstr{$cmd}\n";
	} else {
		print "\t$cmd\t(no help available)\n";
	}
	return 1;
}

sub all_help($ @)
{
	my ($self, @args) = @_;
	my ($cmd);

	if (!@args) {
		foreach $cmd (sort(keys %commands)) {
			all_help_cmd($self, $cmd, 0);
		}
		return 1;
	}
	if (defined($commands{$args[0]})) {
		all_help_cmd($self, $args[0], 1);
	} else {
		$self->warn("Invalid command '$args[0]'");
		return $self->qfatal(1);
	}
	return 1;
}

sub add_migration($ $)
{
	my ($self, $mig) = @_;

	push @{$self->{'MIG'}}, $mig;
	if (@{$self->{'MIG'}} == 1) {
		$self->{'CMIG'} = $self->{'MIG'}[0];
	}
	return $self->rescan_products();
}

sub all_show_one($ $ $)
{
	my ($self, $var, $single) = @_;

	if (!exists($settings{$var})) {
		$self->warn("Undefined variable '$var'");
		return $self->qfatal(1);
	} else {
		print "\t$var\t$settings{$var}\n";
	}
	return 1;
}

sub all_show($ @)
{
	my ($self, @args) = @_;
	my $v;

	if (!@args) {
		foreach $v (sort keys %settings) {
			all_show_one($self, $v, 0) or return 0;
		}
		return 1;
	}
	return all_show_one($self, $args[0], 1);
}

sub all_set($ @)
{
	my ($self, @args) = @_;

	if ($#args == 0 && $args[0] =~ /^(.*?)=(.*)$/) {
		@args = ($1, $2);
		$self->debug(1, "split args into (".join(', ', @args).")\n");
	}
	if ($#args != 1) {
		$self->warn("Usage: set variable value");
		return $self->qfatal(1);
	}
	if (!exists($settings{$args[0]})) {
		$self->warn("Undefined variable $args[0]");
		return $self->qfatal(1);
	}
	$settings{$args[0]} = $args[1];
	print "\t$args[0] -> $args[1]\n";
	return 1;
}

sub top_select($ @)
{
	my ($self, @args) = @_;
	my ($sel, $m, $save, $res);

	if (!@args) {
		$self->warn("Usage:\tselect migname [update]\n\tselect all");
		return $self->qfatal(1);
	}

	$sel = shift @args;
	if ($sel eq 'all') {
		foreach $m (@{$self->{'MIG'}}) {
			$self->{'CMIG'} = $m;
			$res = $self->mig_select('all');
			$self->{'CMIG'} = undef;
			return $res unless ($res);
		}
		return 1;
	}
	
	$m = $self->select_mig($sel);
	if (!defined($m)) {
		$self->warn("Invalid migration specified - $sel");
		return $self->qfatal(1);
	}
	$self->{'CMIG'} = $m;
	if (@args) {
		$res = $self->mig_select(@args);
	} else {
		$res = $self->mig_select('all');
	}
	$self->{'CMIG'} = undef;
	return $res;
}

sub top_unselect($ @)
{
	my ($self, @args) = @_;
	my ($sel, $m, $save, $res);

	if (!@args) {
		$self->warn("Usage:\tunselect migname [update]\n\tunselect all");
		return $self->qfatal(1);
	}

	$sel = shift @args;
	if ($sel eq 'all') {
		foreach $m (@{$self->{'MIG'}}) {
			$self->{'CMIG'} = $m;
			$res = $self->mig_unselect('all');
			$self->{'CMIG'} = undef;
			return $res unless ($res);
		}
		return 1;
	}
	
	$m = $self->select_mig($sel);
	if (!defined($m)) {
		$self->warn("Invalid migration specified - $sel");
		return $self->qfatal(1);
	}
	$self->{'CMIG'} = $m;
	if (@args) {
		$res = $self->mig_unselect(@args);
	} else {
		$res = $self->mig_unselect('all');
	}
	$self->{'CMIG'} = undef;
	return $res;
}

sub mig_select($ @)
{
	my ($self, @args) = @_;

	return $self->mig_real_select('select', 1, @args);
}

sub mig_unselect($ @)
{
	my ($self, @args) = @_;

	return $self->mig_real_select('unselect', 0, @args);
}

sub mig_real_select($ $ $ @)
{
	my ($self, $name, $val, @args) = @_;
	my ($sel, $u);
	my (@all, @selected);

	if (!@args) {
		$self->warn("Usage:\t$name update\n\t$name all");
		return $self->qfatal(1);
	}

	$sel = shift @args;
	if ($sel eq 'all') {
		foreach $u ($self->{'CMIG'}->updates()) {
			if ($u->select($val) != $val) {
				# Oops!
				$self->warn("Failed to $name $u->{version}");
				return 0;
			}
		}
	} else {
		@all = $self->{'CMIG'}->updates();
		@selected = grep { $_->{'version'} eq $sel } @all;
		push @selected,
		    grep { $_->{'product'}.'-'.$_->{'version'} eq $sel } @all;
		if (!@selected) {
			$self->warn("No updates matching $sel");
			return $self->qfatal(1);
		}
		foreach $u (@selected) {
			if ($u->select($val) != $val) {
				# Oops!
				$self->warn("Failed to $name $u->{version}");
				return 0;
			}
		}
	}
	return 1;
}

sub all_testconn($ @)
{
	my ($self, @args) = @_;
	my ($dbh, $err);

	$dbh = $self->dbh(\$err, @args);
	if (defined($dbh)) {
		print "Connection successful!\n";
		$dbh->disconnect();
	} else {
		$self->warn("Connection failed: $err");
		return $self->qfatal(1);
	}
	return 1;
}

sub all_info($ @)
{
	my ($self, @args) = @_;
	my ($dbh, $err, $res, $p, @row);

	$dbh = $self->dbh(\$err, @args);
	if (!defined($dbh)) {
		$self->warn("Connection failed: $err");
		return $self->qfatal(1);
	}
	$res = $self->get_info($dbh);
	if (!defined($res)) {
		# get_info() has already displayed an error
		$dbh->disconnect();
		return 1;
	}

	print "\tProduct\tVersion\tComment\tDate\n";
	foreach $p (sort keys %{$res}) {
		@row = @{$res->{$p}};
		print join("\t", '', @row[0..$#row - 1])."\n";
	}
	$dbh->disconnect();
	return 1;
}

sub all_prepare($ @)
{
	my ($self, @args) = @_;
	my ($dbh, $err, $def, $q, $sth, $ok);
	
	$dbh = $self->dbh(\$err, @args);
	if (!defined($dbh)) {
		$self->warn("Connection failed: $err");
		return $self->qfatal(1);
	}
	$def = $self->get_info($dbh);
	if (defined($def)) {
		print "The ".$self->dbtable()." table already exists.\n";
		$dbh->disconnect();
		return 1;
	}

	if ($settings{'dbschema'}) {
		eval {
			$dbh->do("CREATE SCHEMA $settings{dbschema}");
		};
	}
	$def = $tabledefs{$settings{'dbdriver'}} || $tabledefs{'GENERIC'};
	$q = 'CREATE TABLE '.$self->dbtable().$def;
	$self->debug(1, "trying to $q\n");
	$ok = 0;
	eval {
		$ok = 1 if ($dbh->do($q));
	};
	if (!$ok) {
		if ($@) {
			$self->warn("Error creating table: $@");
		} elsif ($dbh->errstr()) {
			$self->warn("Error creating table: ".$dbh->errstr());
		} else {
			$self->warn("Creating the table failed!");
		}
		$dbh->disconnect();
		$ok = $self->qfatal(1);
	} else {
		print "Successfully created the ".$self->dbtable()." table.\n";
		$ok = 1;
	}
	$dbh->disconnect();
	return $ok;
}

sub top_exam($ @)
{
	my ($self, @args) = @_;
	my ($dbh, $err, $res, $sel, $mig, $r);

	if (!@args) {
		$self->warn("Usage:\texamine migration\n\texamine all");
		return $self->qfatal(1);
	}
	if (!@{$self->{'MIG'}}) {
		$self->warn("No migrations to examine.");
		return $self->qfatal(1);
	}
	$sel = shift @args;
	if ($sel ne 'all') {
		$mig = $self->select_mig($sel);
		if (!defined($mig)) {
			$self->warn("Invalid migration $sel");
			return $self->qfatal(1);
		}
	} else {
		$mig = undef;
	}
	
	$dbh = $self->dbh(\$err);
	if (!defined($dbh)) {
		$self->warn("Connection failed: $err");
		return $self->qfatal(1);
	}
	$res = $self->get_info($dbh);
	$dbh->disconnect();
	return $self->qfatal(1) unless (defined($res));
	
	if (defined($mig)) {
		$self->mig_scan($mig, $res) or return $self->qfatal(1);
		$r = $self->mig_exam_real($mig, $res);
	} else {
		$r = 1;
		foreach $mig (@{$self->{'MIG'}}) {
			$self->mig_scan($mig, $res) or return $self->qfatal(1);
		}
		foreach $mig (@{$self->{'MIG'}}) {
			if (!$self->mig_exam_real($mig, $res)) {
				$r = 0;
				last;
			}
		}
	}
	return $r;
}

sub mig_exam($ @)
{
	my ($self, @args) = @_;
	my ($dbh, $err, $res, $sel, $mig, $r);

	$dbh = $self->dbh(\$err);
	if (!defined($dbh)) {
		$self->warn("Connection failed: $err");
		return $self->qfatal(1);
	}
	$res = $self->get_info($dbh);
	$dbh->disconnect();
	return $self->qfatal(1) unless (defined($res));
	
	return $self->qfatal(1) unless $self->mig_scan($self->{'CMIG'}, $res);

	$r = $self->mig_exam_real($self->{'CMIG'}, $res);
	return $r;
}

sub mig_exam_real($ $ $)
{
	my ($self, $mig, $res) = @_;
	my ($u, $product);
	
	foreach $u ($mig->updates()) {
		$self->debug(1, "testing $u->{product} - $u->{version}\n");
		$product = $u->{'product'};
		if (!defined($res->{$product})) {
			$self->warn("Internal error: product $product");
			if ($self->{'FATAL'}) {
				$self->{'FATALERR'} = 1;
				return 0;
			}
			$u->select(0);
			next;
		}
		$u->select($u->{'version'} gt $res->{$product}->[1]);
	}
	return 1;
}

sub mig_scan($ $ $)
{
	my ($self, $mig, $res) = @_;
	my ($u, $p, $n, $ver);
	
	foreach $u ($mig->updates()) {
		$p = $u->{'product'};
		next if (defined($res->{$p}));
		if (!defined($self->{'PRODUCTS'}->{$p})) {
			$self->warn("Internal error: product $p unscanned");
			if ($self->{'FATAL'}) {
				$self->{'FATALERR'} = 1;
				return 0;
			}
			next;
		}
		if ($self->{'BATCH'}) {
			$ver = 0;
		} else {
			print "Product $p is not described in the database!\n";
			print "Available updates:\n";
			foreach $n (sort keys %{$self->{'PRODUCTS'}->{$p}}) {
				next if $n eq '0';
				$u = $self->{'PRODUCTS'}->{$p}->{$n};
				printf("\t%s %s-%s %s\n", $u->{'selected'},
				    $u->{'product'}, $u->{'version'}, $u->{'comment'});
			}
			do {
				$| = 1;
				print "Please enter the current database version, or hit Enter to exit: ";
				$ver = <STDIN>;
				return 0 unless (defined($ver));
				$ver =~ s/[\r\n]*$//g;
				return 0 if ($ver eq '');
			} while (!exists($self->{'PRODUCTS'}->{$p}->{$ver}) &&
			    $ver ne '0');
		}
		if (defined($self->{'PRODUCTS'}->{$p}->{$ver})) {
			$res->{$p} = [ $p, $ver,
			    $self->{'PRODUCTS'}->{$p}->{$ver}->{'comment'}, '', 1 ];
		} else {
			$res->{$p} = [ $p, $ver, '', '', 1 ];
		}
		$self->debug(1, "set res{$p}=('".join("', '", @{$res->{$p}})."')\n");
	}
	return 1;
}

sub rescan_products($)
{
	my ($self) = @_;
	my ($m, $u, $p);

	$self->{'PRODUCTS'} = { };
	foreach $m (@{$self->{'MIG'}}) {
		foreach $u ($m->updates()) {
			$p = $u->{'product'};
			if (!exists($self->{'PRODUCTS'}->{$p})) {
				$self->{'PRODUCTS'}->{$p} = { };
			}
			$self->{'PRODUCTS'}->{$p}->{$u->{'version'}} = $u;
		}
	}
	return 1;
}

sub all_testrun($ @)
{
	my ($self, @args) = @_;

	$self->{'TEST_RUN'} = 1;
	return $self->all_run(@args);
}

sub all_run($ @)
{
	my ($self, @args) = @_;
	my ($dbh, $err, $res, $started, $p, $pn, $u, $un);

	$dbh = $self->dbh(\$err, @args);
	if (!defined($dbh)) {
		$self->warn("Connection failed: $err");
		return $self->qfatal(1);
	}
	$res = $self->get_info($dbh);

	$started = 0;
	$err = '';
	eval {
		if ($self->{'TEST_RUN'}) {
			$self->debug(1, "\tstarting test run transaction\n");
			$dbh->begin_work() or
			    die('Starting test run transaction: '.$dbh->errstr()."\n");
			$started = 1;
		}
		foreach $pn (sort keys %{$self->{'PRODUCTS'}}) {
			$self->debug(1, "\thandling product $pn\n");
			$p = $self->{'PRODUCTS'}->{$pn};
			$err = '';
			foreach $un (sort keys %{$p}) {
				next if ($un eq '0');
				$u = $p->{$un};
				next if ($u->{'selected'} eq ' ');

				if (!$self->{'TEST_RUN'}) {
					$dbh->begin_work()
					    or die('Starting transaction: '.$dbh->errstr()."\n");
					$started = 1;
				}
	
				$self->debug(1, "\t\tupdate $un\n");
				$err = $u->apply($dbh);
				if ($err) {
					die("Applying update $un for product $pn: $err\n");
				}
				$err = $self->update_info($dbh, $u, $res);
				if ($err) {
					die("Updating the database info for update $un for product $pn: $err\n");
				}
				if (!$self->{'TEST_RUN'}) {
					$dbh->commit()
					    or die('Committing transaction: '.$dbh->errstr()."\n");
					$started = 0;
					$u->select(0);
				}
			}
			last if ($err);
			print "The updates for product $pn were applied successfully!\n";
		}
		if ($self->{'TEST_RUN'}) {
			$self->debug(1, "\trolling back test run transaction\n");
			$dbh->rollback() or
			    die('Rolling back test run transaction: '.$dbh->errstr()."\n");
			$started = 0;
		}
	};
	$err = $@ if ($@);
	if ($err) {
		eval { $dbh->rollback(); } if $started;
		chomp $err;
		$self->warn("$err");
	}
	$self->{'TEST_RUN'} = 0;
	eval { $dbh->disconnect(); };
	return $self->qfatal(1) if $err;
	print "All updates were applied successfully!\n";
	return 1;
}

sub all_load($ @)
{
	my ($self, @args) = @_;
	my ($fname, $basename, $mig, $count, $err);

	if (!@args) {
		$self->warn("Usage: load fname...");
		return $self->qfatal(1);
	}

	$count = 0;
	foreach $fname (@args) {
		$basename = ::basename($fname);
		$self->debug(1, "testing for fname $fname, basename $basename\n");
		$err = undef;
		foreach $mig (@{$self->{'MIG'}}) {
			$self->debug(1, "filename is $fname\n");
			$self->debug(1, "basename is $basename\n");
			$self->debug(1, "mig filename is ".$mig->filename()."\n");
			$self->debug(1, "mig basename is ".$mig->basename()."\n");
			if ($mig->filename() eq $fname) {
				print STDERR
				    "Migration $fname already loaded\n";
				$err = 1;
			} elsif ($mig->basename() eq $basename) {
				print STDERR
				    "A migration with a basename $basename is already loaded\n";
				$err = 1;
			}
		}
		if ($err) {
			if ($self->{'FATAL'}) {
				$self->{'FATALERR'} = 1;
				return 0;
			}
			next;
		}
		
		$err = undef;
		eval {
			$mig = DB::Update::Parse::parse($fname);
		};
		$err = $@ if ($@);
		$err = "unknown error" if (!$mig && !$err);
		if ($err) {
			$self->warn("Error loading $fname: $err");
			if ($self->{'FATAL'}) {
				$self->{'FATALERR'} = 1;
				return 0;
			}
			next;
		}
		$mig->filename($fname);
		$self->add_migration($mig);
		$count++;
	}
	if ($count == @args) {
		print "$count migrations loaded.\n";
	} elsif ($count) {
		print "Only $count of ".scalar(@args)." migrations loaded.\n";
	} else {
		print "No new migrations loaded.\n";
	}
	return 1;
}

sub all_unload($ @)
{
	my ($self, @args) = @_;
	my ($n, $i, $count);

	if (!@args) {
		$self->warn("Usage: unload fname | idx...");
		if ($self->{'FATAL'}) {
			$self->{'FATALERR'} = 1;
			return 0;
		}
	}
	$count = 0;
	foreach $n (@args) {
		$self->debug(1, "unload loop, n is $n, migs is '".join("', '", @{$self->{'MIG'}})."'\n");
		for ($i = 0; $i < @{$self->{'MIG'}}; $i++) {
			last if ($i + 1 eq $n ||
			    $self->{'MIG'}->[$i]->filename() eq $n ||
			    $self->{'MIG'}->[$i]->basename() eq $n);
		}
		$self->debug(1, "i is $i, count is ".scalar(@{$self->{'MIG'}})."\n");
		if ($i == @{$self->{'MIG'}}) {
			$self->warn("Invalid migration $n");
			if ($self->{'FATAL'}) {
				$self->{'FATALERR'} = 1;
				return 0;
			}
			next;
		}
		$self->{'MIG'}->[$i]->reset();
		splice(@{$self->{'MIG'}}, $i, 1);
#		delete $self->{'MIG'}->[$i];
		$self->debug(1, "unload loop after del, n is $n, migs is '".join("', '", @{$self->{'MIG'}})."'\n");
		$count++;
	}
	$self->rescan_products() if $count;
	if ($count == @args) {
		print "$count migrations unloaded.\n";
	} elsif ($count) {
		print "Only $count of ".scalar(@args)." migrations unloaded.\n";
	} else {
		print "No migrations unloaded.\n";
	}
	if ($count) {
		$self->{'CUPD'} = $self->{'CMIG'} = undef
	}
	return 1;
}

sub all_reload($ @)
{
	my ($self, @args) = @_;
	my ($count, $n, $m, @names) = @_;

	if (!@args) {
		$self->warn("Usage: reload fname | idx...");
		return $self->qfatal(1);
	}

	foreach $n (@args) {
		$m = $self->select_mig($n);
		if (!defined($m)) {
			$self->warn("Invalid migration $n");
			if ($self->{'FATAL'}) {
				$self->{'FATALERR'} = 1;
				return 0;
			}
			next;
		}
		push @names, $m->filename();
		$count++;
	}
	if ($count == 0) {
		$self->warn("No migrations matched!");
		return $self->qfatal(1);
	}
	$self->warn("$count migrations scheduled for reloading");
	return 0 unless $self->all_unload(@names);
	return 0 unless $self->all_load(@names);
	return 1;
}

sub update_info($ $ $)
{
	my ($self, $dbh, $u, $res) = @_;
	my ($pn, $q, $sth);

	if (!defined($self->{'SQL'}->{'update_info'})) {
		$q = 'UPDATE '.$self->dbtable().' '
		    .'SET VerVersion = ?, VerComment = ?, VerUpdated = NOW() '
		    .'WHERE VerProduct = ?';
		$sth = $dbh->prepare($q);
		die "Preparing version update query: ".$dbh->errstr()."\n"
		    unless (defined($sth));
		$self->{'SQL'}->{'update_info'} = $sth;
	}
	if (!defined($self->{'SQL'}->{'update_info_add'})) {
		$q = 'INSERT INTO '.$self->dbtable()
		    .'   (VerProduct, VerVersion, VerComment, VerUpdated) '
		    .'VALUES (?, ?, ?, NOW())';
		$sth = $dbh->prepare($q);
		die "Preparing version insert query: ".$dbh->errstr()."\n"
		    unless (defined($sth));
		$self->{'SQL'}->{'update_info_add'} = $sth;
	}

	$pn = $u->{'product'};
	if (defined($res->{$pn})) {
		$self->debug(1, "res(pn) is ".$res->{$pn}."\n");
	} else {
		$self->debug(1, "res(pn) not defined\n");
	}
	eval {
		if (defined($res->{$pn}) && $res->{$pn}->[0] ne '0') {
			$self->debug(1, "values are ('".join("', '", @{$res->{$pn}})."')\n");
			$sth = $self->{'SQL'}->{'update_info'};
			$sth->execute($u->{'version'}, $u->{'comment'},
			    $u->{'product'});
		} else {
			$self->debug(1, "inserting\n");
			$sth = $self->{'SQL'}->{'update_info_add'};
			$sth->execute($u->{'product'}, $u->{'version'},
			    $u->{'comment'});
		}
	};
	die("database error: $@") if ($@);
	$res->{$pn}->[0] = $u->{'version'};
	return '';
}

=pod

=back

=head1 AUTHORS

The C<DB::Update::Shell> module was written by Peter Pentchev
<roam@ringlet.net>.

=cut

1;
