#!/usr/bin/perl -Tw

use strict;

package DB::Update::Single;

use DB::Update::Output qw/:dbo :debug/;

=pod

=head1 NAME

DB::Update::Single - manipulate a single database update

$Ringlet$

=head1 SYNOPSIS

    use DB::Update::Single;

    $u = new DB::Update::Single;
    $u->set('product', 'Updater');
    $u->set('version', '1.00');
    $u->set('comment', 'Initial database version');

    $u->add_batch($b1);
    $u->add_batch($b2);

    display_batch($_) for ($u->batches());

    eval {
        $u->apply($dbh);
    };
    print "Update failed: $@\n" if ($@);

=head1 DESCRIPTION

C<DB::Update::Single> is a class for handling incremental SQL database
schema updates as represented by the C<db-update> element in the DBXML file
format.  Each update contains the SQL queries necessary to bring the
database schema of a particular product up-to-date with the state of the
product's other code.  The updates are differentiated by product name and by
update versions.

Each update contains one or more query batches (C<sql-batch> elements,
C<DB::Update::Batch> objects), each of which in turn contains one or more
data definition SQL queries (C<sql-query> elements, C<DB::Update::Query>
objects).

=head1 MEMBERS

Each C<DB::Update::Single> object contains the following hash members.  Most
of them correspond directly to the DBXML file format members:

=over 4

=item product

The name of the software product that this database schema update is
targeted at.

=item version

The database schema version of the update.

=item comment

A one-line comment outlining the purpose of the update.

=item desc

A longer description of the update (corresponds to the C<comments> DBXML
element).

=item selected

A flag set if the user has selected this update for execution.

=back

=head1 CONSTRUCTOR

=over 4

=item new

This is the constructor for a new DB::Update::Single object.  It takes no
arguments in the present version.

=back

=cut

sub new
{
	my $proto = shift;
	my $class = ref($proto) || $proto;
	my $self = { };

	$self->{'product'} = undef;
	$self->{'version'} = undef;
	$self->{'comment'} = undef;
	$self->{'desc'} = undef;
	$self->{'selected'} = ' ';
	$self->{'batches'} = [ ];
	bless $self, $class;
	return $self;
}

=pod

=head1 METHODS

=over 4

=item set (ATTR, VAL)

Set a particular member of the C<DB::Update::Single> class; see L</MEMBERS>
above for more information.

=cut

sub set($ $ $)
{
	my ($self, $attr, $val) = @_;

	die("Invalid update attribute '$attr'")
	    unless (exists($self->{$attr}));
	if ($attr eq 'version' && $val =~ /^\d+$/) {
		$val = "0.$val";
	}
	$self->{$attr} = $val;
}

=pod

=item select ([BOOL])

Get or set the 'selected for applying' flag (see C<selected> in the
L</MEMBERS> section above).

=cut

sub select($ $)
{
	my ($self, $val) = @_;

	if (defined($val)) {
		$self->{'selected'} = $val? 'X': ' ';
	}
	return $self->{'selected'} eq 'X';
}

=pod

=item update (DBH)

Attempt to apply the update, executing all of its data definition SQL
queries in succession over the specified database handle C<DBH>.  If any of
the queries should produce an error while executing, the C<update()> method
dies with a descriptive error message.

=cut

sub apply($ $)
{
	my ($self, $dbh) = @_;
	my ($b, $q);

	debug(1, "attempting to apply update ".$self->{'product'}.'-'.$self->{'version'}."\n");
	foreach $b ($self->batches()) {
		debug(1, "attempting to apply batch ".$b->id()."\n");
		foreach $q ($b->queries()) {
			debug(1, "attempting query ".$q->name());
			debug(2, "\tQuery text:\n".$q->text()."\n====\n");
			if (!$dbh->do($q->text())) {
				die sprintf("batch %s, query %s: %s\n",
				    $b->id(), $q->name(),
				    $dbh->errstr());
			}
		}
	}
	return 0;
}

=pod

=item add_batch (BATCH)

Add a query batch at the end of the update's list of batches.  The batch
should be an object derived from the C<DB::Update::Batch> class.

=cut

sub add_batch($ $)
{
	my ($self, $batch) = @_;

	push @{$self->{'batches'}}, $batch;
}

=pod

=item batches

Return a list of the batches in this update as C<DB::Update::Batch>
objects.

=cut

sub batches($)
{
	my ($self) = @_;

	return @{$self->{'batches'}};
}

=pod

=item reset

Free all resources allocated for the current update and its batches.

=cut

sub reset($)
{
	my ($self) = @_;

	$self->{'product'} = $self->{'version'} = $self->{'comment'} =
	    $self->{'desc'} = $self->{'selected'} = undef;
	$_->reset() for (@{$self->{'batches'}});
	$self->{'batches'} = undef;
}

=pod

=item output

Output a DBXML representation of the current update.

=cut

sub output($)
{
	my ($self) = @_;

	dbo_output(sprintf(
	    '  <db-update product="%s" version="%s" comment="%s">',
	    dbo_escape($self->{'product'}, $self->{'version'}, $self->{'comment'})));
	if ($self->{'desc'} && length($self->{'desc'}) > 0) {
		dbo_output('    <comments>');
		print "-- $_\n" for dbo_escape(split(/\n/, $self->{'desc'}));
		dbo_output('    </comments>');
	}
	$_->output() for @{$self->{'batches'}};
	dbo_output('  </db-update>');
}

=pod

=back

=head1 SEE ALSO

C<DB::Update::Migration>, C<DB::Update::Batch>, C<DB::Update::Query>,
C<DB::Update::Shell>

=head1 AUTHORS

The C<DB::Update::Single> module was written by Peter Pentchev
<roam@ringlet.net> in 2004.

=cut

1;
