#!/usr/bin/perl -Tw

use strict;

package DB::Update::Output;

=pod

=head1 NAME

DB::Update::Output - miscellaneous output-related routines

$Ringlet$

=head1 SYNOPSIS

   # DBXML output routines

   use DB::Update::Output qw/:dbo/;

   Output a line with the proper DBXML prefix:

   dbo_output('<db-migration>');
   dbo_output(sprintf('  <db-update product="%s" version="%s" comment="%s">',
       dbo_escape($product, $version, $comment)));
   print join("\n", dbo_escape("-- The contents of the update go here."));
   dbo_output('  </db-update>');
   dbo_output('</db-migration>');

   # Debugging routines

   use DB::Update::Output qw/:debug/;

   debug_enable(1);
   debug_threshold(5);
   debug(3, "A diagnostic message");
   debug_handler(sub { print STDERR "My debug handler: <$_[0]> $_[1]\n"; });
   debug(1, "A higher-level diagnostic message");

   $something->init_debug() if debug_enable();
   $something->debug_threshold(debug_threshold());

=head1 DESCRIPTION

FIXME: write me up!

=cut

use Exporter;

our @ISA = qw/Exporter/;
our @EXPORT_OK = qw/dbo_escape dbo_output
	debug debug_enable debug_handler debug_threshold/;
our %EXPORT_TAGS = (
		'dbo'	=> [ qw/dbo_escape dbo_output/ ],
		'debug'	=> [ qw/debug debug_enable debug_handler debug_threshold/ ],
	);

my $debug_handler = undef;
my $debug = 0;
my $debug_threshold = 9;

=pod

=head1 FUNCTIONS

The C<DB::Update::Output> module exports two sets of functions.

=head2 DBXML output routines (:dbo)

=over 4

=item dbo_output (STRING)

Output the specified string, prefixing it with the DBXML line prefix
(usually C<-- DBXML:>).

=cut

sub dbo_output($)
{
	my ($line) = @_;

	print "-- DBXML: $line\n";
}

=pod

=item dbo_escape (LIST)

Escape the "dangerous" characters in the specified list of strings, making
it suitable for use in XML attribute values.

=cut

sub dbo_escape(@)
{
	my ($s, @out) = (undef);

	foreach $s (@_) {
		$s =~ s/\&/&amp;/g;
		$s =~ s/</&lt;/g;
		$s =~ s/>/&gt;/g;
		$s =~ s/"/&quot;/g;
		push @out, $s;
	}
	if (@out == 1) {
		return $out[0];
	} else {
		return @out;
	}
}

=pod

=back

=head2 Debugging routines (:debug)

=over 4

=item debug_handler ([HANDLER])

Get or set the current handler for outputting debug information.  The
handler should be a code reference; if set, it is invoked by the B<debug()>
function.

=cut

sub debug_handler($)
{
	my ($h) = @_;

	if (defined($h)) {
		croak("A debug handler must be a code ref")
		    unless (ref($h) eq 'CODE');
		$debug_handler = $h;
	}
	return $debug_handler;
}

=pod

=item debug_threshold ([LEVEL])

Get or set the diagnostic level used to determine whether a message passed
to B<debug()> will be displayed at all.

=cut

sub debug_threshold($)
{
	my ($thresh) = @_;

	$debug_threshold = $thresh if defined($thresh);
	return $debug_threshold;
}

=pod

=item debug (LEVEL MESSAGE)

Display a diagnostic message.  If B<debug_handler()> has been invoked with a
code reference parameter, the message is passed to the handler for
processing.  Otherwise, it is output to the standard error stream, prefixed
by the string C<RDBG > and the diagnostic level, and followed by a newline.

=cut

sub debug($ $)
{
	my ($level, $msg) = @_;

	return unless ($debug && $level <= $debug_threshold);
	if (defined($debug_handler)) {
		&{$debug_handler}($level, $msg);
	} else {
		print STDERR "RDBG <$level> $msg\n";
	}
}

=pod

=item debug_enable ([FLAG])

Enable/disable or just query the current state of displaying diagnostic
messages.

=cut

sub debug_enable($)
{
	my ($flag) = @_;

	$debug = $flag if defined($flag);
	return $debug;
}

=pod

=back

=head1 AUTHORS

The C<DB::Update::Output> module was written by Peter Pentchev
<roam@ringlet.net> in 2004.

=cut

1;
