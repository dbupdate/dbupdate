#!/usr/bin/perl -w

use strict;

package DB::Update::Shell::Console;

# $Ringlet$

our @ISA = qw/DB::Update::Shell/;

sub getline($ $ $ $)
{
	my ($self, $prompt, $mig, $upd) = @_;
	my $line;

	$| = 1;
	print "$prompt> ";
	$line = <STDIN>;
	return $line;
}

sub debug($ $ $)
{
	my ($self, $level, $msg) = @_;

	$msg =~ s/[\r\n]*$//;
	print "RDBG <$level> $msg\n" if $self->{'DEBUG'};
}

sub warn($ $)
{
	my ($self, $msg) = @_;

	$msg =~ s/[\r\n]*$//;
	print STDERR "$msg\n";
}

sub err($ $)
{
	my ($self, $msg) = @_;

	$msg =~ s/[\r\n]*$//;
	print STDERR "$msg\n";
}

1;
