#!/usr/bin/perl -Tw

use strict;

package DB::Update;

=head1 NAME

DB::Update - a framework for migrating a database schema between versions

$Ringlet$

=head1 DESCRIPTION

FIXME: write me up.

=cut

use vars qw($VERSION @ISA);
@ISA = qw();

$VERSION = '0.07';

=head1 AUTHOR

Peter Pentchev, <roam@ringlet.net>

=cut

1;
