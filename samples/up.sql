-- DBXML: <?xml version="1.0" encoding="us-ascii"?>
-- DBXML: <db-migration>
-- DBXML:   <db-update product="p" version="1.0" comment="First try.">
-- DBXML:     <comments>
-- First attempt at creating the 'p' product.
-- 
-- $Ringlet$
-- DBXML:     </comments>
-- DBXML:     <sql-batch id="create-foo">
-- DBXML:       <sql-query name="create-foo-schema">
CREATE SCHEMA foo;
-- DBXML:       </sql-query>
-- DBXML:       <sql-query name="create-foo">
CREATE TABLE foo(a INTEGER NOT NULL, b VARCHAR(20) NOT NULL);
-- DBXML:       </sql-query>
-- DBXML:     </sql-batch>
-- DBXML:     <sql-batch id="populate-foo">
-- DBXML:       <sql-query name="pop-foo-1">
INSERT INTO foo VALUES( 5, 'abc');
-- DBXML:       </sql-query>
-- DBXML:       <sql-query name="pop-foo-2">
INSERT INTO foo VALUES(15, 'abcdef');
-- DBXML:       </sql-query>
-- DBXML:       <sql-query name="pop-foo-3">
INSERT INTO foo VALUES(25, 'abcggu');
-- DBXML:       </sql-query>
-- DBXML:     </sql-batch>
-- DBXML:   </db-update>
-- DBXML:   <db-update product="p" version="1.1" comment="Add the blah table">
-- DBXML:     <sql-batch id="add-blah">
-- DBXML:       <sql-query name="add-blah">
CREATE TABLE blah(a INTEGER NOT NULL);
-- DBXML:       </sql-query>
-- DBXML:     </sql-batch>
-- DBXML:   </db-update>
-- DBXML: </db-migration>
